#!/usr/bin/env python3

myTB = {}
myTB['Robby'] = '030/1230-3478'
myTB['Liesel'] = '00123 / 1234123'
myTB['Mona'] = '+49 (170) 1223'

#print(myTB)
for Name, Nummer in myTB.items():
    print(f'Name: {Name} \t Nummer: {Nummer}')