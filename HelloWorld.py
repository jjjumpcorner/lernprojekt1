#!/usr/bin/env python3

bps = 56000 # Bits pro Sek
Bps = bps/8 # Byte pro Sek
Bpm = Bps * 60 # Bytes pro Minute
Menge = 7 * 1024 ** 2 # in Bytes
Res = Menge / Bpm

print(f'{Res:.2f}')