#!/usr/bin/env python3

import socket

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn as c:
        print('Connected by', addr)
        while True:
            data = c.recv(1024)
            if not data:
                break
            # simple calc funcs
            erg = eval(data.decode("utf-8"))
            c.sendall(erg.encode())